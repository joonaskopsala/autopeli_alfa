﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

    public GameObject Player;
    public Vector3 CameraFollowDistance;
    public Transform target;
    public float distanceUp;
    public float distanceBack;
    public float minimumHeight;

    RaycastHit hit;

    private Vector3 positionVelocity;    
    private GameObject player;
    private bool isHitting;

    // Use this for initialization
    void Start () {

        player = GameObject.FindGameObjectWithTag("Player");
	}

    // Update is called once per frame
    /*void Update () {

        //tansform.position = Player.transform.position + CameraFollowDistance;

	}*/
    void FixedUpdate()
    {

        //calculate cam position
        Vector3 newPosition = target.position + (target.forward * distanceBack);
        newPosition.y = Mathf.Max(newPosition.y + distanceUp, minimumHeight);

        //move camera
        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref positionVelocity, 0.18f);

        //rotate the camera to look at the car
        Vector3 focalPoint = target.position + (target.forward * 5);
        transform.LookAt(focalPoint);


        if (Physics.Raycast(player.transform.position, Vector3.up, out hit) || Physics.Raycast(transform.position, Vector3.up, out hit))
        {


            if (hit.collider.tag == "MapObject")
            {

                newPosition.y = Mathf.Max(newPosition.y - 10, minimumHeight);

                transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref positionVelocity, 0.18f);
                //Debug.Log(hit.collider + "Auto tai kamera raycastHitUp");




            }
        }                    

    }

}
