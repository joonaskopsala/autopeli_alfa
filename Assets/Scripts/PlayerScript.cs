﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerScript : MonoBehaviour
{

    public float acceleration;
    public float rotationRate;
    public float sideSpeed = 25;


    public float turnRotationAngle;
    public float turnRotationSeekSpeed;
    private float turnInput;

    public bool Grounded;

    private float rotationVelocity;
    private float groundAngleVelocity;

    float lockPos = 0;

    public float speed;



    private Rigidbody rb;


    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, lockPos, transform.rotation.eulerAngles.z);
        speed = rb.velocity.magnitude;
    }

    public void PlayerBoost()
    {
        boosterBool = true;

        StartCoroutine(Boostimer(2.0f));
    }

    public bool boosterBool;

    IEnumerator Boostimer(float Wait)
    {
        yield return new WaitForSeconds(Wait);

        boosterBool = false;
    }


    void FixedUpdate()
    {
        print(speed);
        Vector3 sideSpeedNow = transform.right * sideSpeed * Input.GetAxis("Horizontal");

        if (!boosterBool)
        {
            if (Physics.Raycast(transform.position, transform.up * -1, 3f))
            {
                //we are grounded, increasing drag
                rb.drag = 0.2f;

                //calculating forward force
                Vector3 forwardForce = transform.forward * acceleration * Input.GetAxis("Vertical");  //jos haluaa movementin itelle
                //correct force using deltatime and vehicle mass
                forwardForce = forwardForce * Time.deltaTime * rb.mass;

                rb.AddForce(forwardForce);


                sideSpeedNow = sideSpeedNow * Time.deltaTime * rb.mass;
                rb.AddForce(sideSpeedNow);
            }
            else
            {
                rb.drag = 0;

                //turning in air reduced by 80%
                sideSpeedNow = sideSpeedNow * Time.deltaTime * rb.mass * 0.2f;
                rb.AddForce(sideSpeedNow);
            }
        }

        if(boosterBool)
        {
            if (Physics.Raycast(transform.position, transform.up * -1, 3f))
            {
                //we are grounded, increasing drag
                rb.drag = 0.2f;

                //calculating forward force
                Vector3 forwardForce = transform.forward * (acceleration*3) * Input.GetAxis("Vertical");  //jos haluaa movementin itelle
                //correct force using deltatime and vehicle mass
                forwardForce = forwardForce * Time.deltaTime * rb.mass;

                rb.AddForce(forwardForce);


                sideSpeedNow = sideSpeedNow * Time.deltaTime * rb.mass;
                rb.AddForce(sideSpeedNow);
            }
            else
            {
                rb.drag = 0;

                //turning in air reduced by 80%
                sideSpeedNow = sideSpeedNow * Time.deltaTime * rb.mass * 0.2f;
                rb.AddForce(sideSpeedNow);
            }
        }
      


        Vector3 newRotation = transform.eulerAngles;
        newRotation.z = Mathf.SmoothDampAngle(newRotation.z, Input.GetAxis("Horizontal") * -turnRotationAngle, ref rotationVelocity, turnRotationSeekSpeed);
        transform.eulerAngles = newRotation;
        
    }


}
