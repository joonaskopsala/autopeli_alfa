﻿using UnityEngine;
using System.Collections;

public class ThrusterScript : MonoBehaviour {

    public float thrusterStrength;
    public float thrusterDistance;
    public Transform[] thrusters;
    private Rigidbody rb;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        RaycastHit hit;

        foreach(Transform thruster in thrusters)
        {
            Vector3 downforce;
            float distancePercentage;

            if(Physics.Raycast ( thruster.position, thruster.up * -1, out hit, thrusterDistance))
            {
                //thrusters distance from ground
                distancePercentage = 1 - (hit.distance / thrusterDistance);

                //force push calculate
                downforce = transform.up * thrusterStrength * distancePercentage;
                //correct the force mass of the car and deltatime
                downforce = downforce * Time.deltaTime * rb.mass;

                rb.AddForceAtPosition(downforce, thruster.position);
            }
        }
    }
}
