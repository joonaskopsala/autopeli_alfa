﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerCollisionController : MonoBehaviour {

    private PlayerScript playerScript;

	// Use this for initialization
	void Start () {

        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Ground")
        {
            playerScript.Grounded = true;
        }

        if(col.tag == "MapObject")
        {
            Debug.Log("collided a mapobject");
            Death();
        }
    }

    void OnTriggerStay(Collider col)
    {
        if(col.tag == "Ground")
        {
            playerScript.Grounded = true;
        }
    }

    void OnTriggerExit (Collider col)
    {
        if(col.tag == "Ground")
        {
            playerScript.Grounded = false;
        }
    }

   void Death()
    {
        SceneManager.LoadScene("TestiSkene");
    }

}
