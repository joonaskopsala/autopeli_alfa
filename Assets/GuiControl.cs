﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GuiControl : Singleton<GuiControl>
{
    public Text ScoreText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GameManagerScript.DistanceScore < 0)
        {
            ScoreText.text = (GameManagerScript.DistanceScore * -1).ToString();
        }
        else
        {
            ScoreText.text = "0";
        }
	
	}
}
