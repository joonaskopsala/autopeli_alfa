﻿using UnityEngine;
using System.Collections;

public class BoosterObjectScript : MonoBehaviour {

    private PlayerScript playerScript;

	// Use this for initialization
	void Start () {

        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Destroy(gameObject);
            playerScript.PlayerBoost();
            print("boostoo");
        }
    }
}
