﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    public static T instance;
    public bool isPersistant;

    public virtual void Awake()
    {
        if (isPersistant)
        {
            if (!instance)
            {
                instance = this as T;
            }
            else {
                DestroyObject(transform.gameObject);
            }
            DontDestroyOnLoad(transform.gameObject);
        }
        else {
            instance = this as T;
        }
    }
}