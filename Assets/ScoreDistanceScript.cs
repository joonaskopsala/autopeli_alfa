﻿using UnityEngine;
using System.Collections;

public class ScoreDistanceScript : MonoBehaviour {

    private GameObject Player;
    public float Distance;

	// Use this for initialization
	void Start () {

        Player = GameObject.Find("Player");
	
	}
	
	// Update is called once per frame
	void Update () {

        Distance = this.transform.position.z - Player.transform.position.z;
        GameManagerScript.DistanceScore = Distance;

	
	}
}
