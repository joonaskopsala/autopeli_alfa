﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; //listiä varten
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using UnityEngine.SceneManagement;

public class GameManagerScript : Singleton<GameManagerScript> {

    public static float DistanceScore;

    void Start()
    {
        //MainMenu scene hakee aina tiedot leveleistä
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            //mitä main menussa tehdää
        }
        else
        {
            SceneManager.LoadScene("GUIScene", LoadSceneMode.Additive);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
